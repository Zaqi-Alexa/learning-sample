<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('login', 'Api\Auth\UserController@login');
Route::post('register', 'Api\Auth\UserController@register');

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['middleware' => 'auth:api'], function(){

	Route::group(['prefix' => 'subjects'], function () {
		Route::get('/', 'Api\General\SubjectController@index');
		Route::get('/{id}', 'Api\General\SubjectController@show');
		Route::get('/list-inactive', 'Api\General\SubjectController@inActive');
		Route::get('/subject-homepage', 'Api\General\SubjectController@mySubjectHomepage');
		Route::get('/my-subjects', 'Api\General\SubjectController@mySubject');
		Route::put('/update/{id}', 'Api\General\SubjectController@store');
    });

    Route::group(['prefix' => 'courses'], function () {
		Route::get('/', 'Api\General\CourseController@index');
		Route::get('/{id}', 'Api\General\CourseController@show');
		Route::get('/subject/{id}', 'Api\General\CourseController@courseByCategory');
		Route::get('/view/{id}', 'Api\General\CourseController@viewers');
		Route::post('/store', 'Api\General\CourseController@store');
		Route::post('/update/{id}', 'Api\General\CourseController@store');
    });

    Route::group(['prefix' => 'general'], function () {
		Route::get('/auth', 'Api\General\GeneralController@auth');
		Route::get('/search', 'Api\General\GeneralController@searchCourse');
    });

});
