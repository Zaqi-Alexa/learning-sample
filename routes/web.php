<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();

Route::group(['middleware' => ['auth']], function () { 
	Route::get('/', 'Backend\CourseController@index')->name('/');
	Route::get('subjects/', 'Backend\CourseController@subjects');
	Route::get('subject', 'Backend\CourseController@bySubject');
	Route::get('explore/', 'Backend\CourseController@explore');
	Route::get('subscription/', 'Backend\CourseController@subscription');
	Route::get('course/watch', 'Backend\CourseController@video');
	Route::post('course/subscribe/', 'Backend\CourseController@subscribe');
	Route::post('course/subjects/', 'Backend\CourseController@followSubjects');
	Route::post('course/liked/', 'Backend\CourseController@liked');
	Route::post('course/comment/', 'Backend\CourseController@comment');
	Route::get('channel/{uniqid}', 'Backend\CourseController@channel');
});

Route::get('/home', 'HomeController@index')->name('home');
