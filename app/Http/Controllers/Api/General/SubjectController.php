<?php

namespace App\Http\Controllers\Api\General;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Subject;
use App\Models\UserSubject;
use Validator;

class SubjectController extends Controller
{
    public function index()
    {
    	$data = Subject::where('is_active', 1)->get();
    	return $data;
    }

    public function mySubjectHomepage()
    {
        $data = UserSubject::with('subject')->where('user_id', auth()->user()->id)->limit(6)->get();
        return response()->success('Success', $data);
    }

    public function mySubject()
    {
        $data = UserSubject::with('subject')->where('user_id', auth()->user()->id)->get();
        return response()->success('Success', $data);
    }

    public function show($id)
    {
    	$data = Subject::find($id);
    	return $data;
    }

    public function inActive()
    {
    	$data = Subject::where('is_active', 0)->get();
    	return $data;
    }
}
