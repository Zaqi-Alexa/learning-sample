<?php

namespace App\Http\Controllers\Api\General;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Course;
use App\Models\Subject;
use Validator;
use DB;

class CourseController extends Controller
{
	public function index()
    {
    	$data = Course::where('is_active', 1)->get();
    	return response()->success('Success', $data);
    }

    public function show($id)
    {
    	$data = Course::find($id);
    	return response()->success('Success', $data);
    }

    public function viewers($id)
    {
    	$data = Course::find($id);
    	$count = $data->views + 1;
    	$data->update([ 'views' => $count ]);

    	return response()->success('Success');
    }

    public function courseByCategory($id)
    {
    	$course = Course::where('subject_id', $id)->where('is_active',1)->get();
    	return response()->success('Success', $course);
    }

    public function store(Request $request, $id='')
    {
    	$validator = Validator::make($request->all(), [ 
    		'name' => 'required', 
    		'type' => 'required', 
    		'level' => 'required', 
    		'description' => 'required', 
    		'subject_id' => 'required', 
    		'is_active' => 'required', 
    	]);

    	if ($validator->fails()) { 
    		return response()->error('Something went wrong', $validator->errors());  
    	}

    	DB::beginTransaction();
    	try {
    		$input = $request->all(); 
    		$input['updloaded_by'] = auth()->user()->id;
    		$symbol = array("/", " ", ".", "*");

    		if($request->hasFile('icon')) {
    			$validator = Validator::make($request->all(), [
    				'icon' =>  'mimes:jpeg,jpg,png|max:100040|required'
    			]);

    			if($validator->fails()){
    				return response()->error('Something went wrong', $validator->errors());
    			}

    			$icon = $request->icon;
                $nameIcon = str_replace($symbol, '_', $request->name);
                $input['icon'] = time().$nameIcon.'.'.$icon->getClientOriginalExtension();
                $destinationIcon = public_path('icon/');
                $icon->move($destinationIcon, $input['icon']);
		    }

		    if($request->hasFile('value')) {
    			$validator = Validator::make($request->all(), [
    				'value' =>  'mimes:mpeg,ogg,mp4,webm,3gp,mov,flv,avi,wmv,ts,pdf,PDF,docs,csv,txt,zip,pptx|max:100040|required'
    			]);

    			if($validator->fails()){
    				return response()->error('Something went wrong', $validator->errors());
    			}

    			$value = $request->value;
                $nameValue = str_replace($symbol, '_', $request->name);
                $input['value'] = time().$nameValue.'.'.$value->getClientOriginalExtension();
                $destinationValue = public_path('courses/');
                $value->move($destinationValue, $input['value']);
		    }

	        if ($id) {
	        	$data = Course::find($id)->update($input);
	        } else {
	        	$data = Course::create($input);
	        }

	        DB::commit();
	        return response()->success('Success', $data);
    	} catch (Exception $e) {
    		DB::rollback();
    		return response()->error('Something went wrong', $e->getMessage());    
    	}
    }

}
