<?php

namespace App\Http\Controllers\Api\General;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Course;
use Validator;
use DB;

class GeneralController extends Controller
{
    public function auth()
    {
    	$user = auth()->user();
    	return response()->success('Success', $user);
    }

    public function searchCourse(Request $request)
    {
    	if (empty($request->key) && empty($request->subject_id)) {
    		return response()->success('Success', NULL);
    	}

    	$query = Course::where('name', 'LIKE', "%$request->key%");
    	if ($request->subject_id) {
    		$query->where('subject_id', $request->subject_id);
    	}

    	$data = $query->get();

    	if (count($data) == 0) {
	    	return response()->success('Success', NULL);
    	}

    	return response()->success('Success', $data);
    }
}
