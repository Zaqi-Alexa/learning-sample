<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use DB;
use Auth;
use App\User;
use App\Models\Subject;
use App\Models\Course;
use App\Models\Banner;
use App\Models\Subscribe;
use App\Models\Like;
use App\Models\Viewer;
use App\Models\Comment;
use App\Models\UserSubject;

class CourseController extends Controller
{
    public function index()
    {
        $banners = Banner::where('is_active', true)->get();
        $checkSubject = UserSubject::where('user_id', Auth::id())->pluck('subject_id');
        $channel = User::select('id', 'name', 'avatar', 'uniqid')
                                ->where('is_uploader', true)
                                ->with(array('course' => function($query){
                                    $query->select('id','name', 'type', 'banner', 'updloaded_by');
                                }))
                                ->inRandomOrder()
                                ->first();
        if (count($checkSubject) != 0) {
            $lists = $checkSubject;
        } else {
            $lists = Subject::pluck('id');
        }

        $recommended = Course::select('courses.id', 'courses.name', 'courses.created_at', 'courses.courseID', 'courses.type', 'courses.banner', 'courses.updloaded_by', DB::raw("count(viewers.id) as viewers"))
                        ->join('viewers', 'courses.id', '=', 'viewers.course_id')
                        ->where('is_active', 1)
                        ->whereIn('subject_id', [$lists])
                        ->with(array('uploader' => function($query){
                            $query->select('id','name', 'avatar', 'uniqid');
                        }))
                        ->groupBy('courses.id')
                        ->inRandomOrder()
                        ->limit(10)
                        ->get();

        $suggest = Course::select('courses.id', 'courses.name', 'courses.created_at', 'courses.courseID', 'courses.type', 'courses.banner', 'courses.updloaded_by', DB::raw("count(viewers.id) as viewers"))
                        ->join('viewers', 'courses.id', '=', 'viewers.course_id')
                        ->where('is_active', 1)
                        ->with(array('uploader' => function($query){
                            $query->select('id','name', 'avatar', 'uniqid');
                        }))
                        ->groupBy('courses.id')
                        ->inRandomOrder()
                        ->limit(9)
                        ->get();

    	return view('apps.course.home', compact('banners', 'recommended', 'channel', 'suggest'));
    }

    public function subjects()
    {
        $subjects = Subject::where('is_active',true)
                    ->with(array('mySubject' => function($query){
                            $query->select('subject_id')->where('user_id',Auth::id())->get();
                        }))->get();
        // return $subjects[0]->mySubject[0];
    	return view('apps.subjects.index', compact('subjects'));
    }

    public function bySubject(Request $request)
    {
        if (!$request->q) {
            return "404";
        }

        $subjects = Course::select('courses.id', 'courses.name', 'courses.type', 'courses.created_at', 'courses.banner', 'courses.updloaded_by', DB::raw("count(viewers.id) as viewers"))
                        ->join('viewers', 'courses.id', '=', 'viewers.course_id')
                        ->where('is_active', 1)
                        ->where('subject_id', $request->q)
                        ->with(array('uploader' => function($query){
                            $query->select('id','name', 'avatar', 'uniqid');
                        }))
                        ->groupBy('courses.id')
                        ->inRandomOrder()
                        ->limit(12)
                        ->get();

    	return view('apps.subjects.list', compact('subjects'));
    }

    public function explore()
    {
        $subjects = Subject::inRandomOrder()->limit(rand(1,6))->pluck('id');
        $checkSubject = UserSubject::where('user_id', Auth::id())->inRandomOrder()->limit(rand(1,6))->pluck('subject_id');
        $viewer = Viewer::where('user_id', Auth::id())->inRandomOrder()->limit(rand(5,8))->pluck('course_id');
        
        if ($checkSubject) {
            $lists = array_unique(array_merge($checkSubject->toArray(), $subjects->toArray()));
        } else {
            $lists = $subjects;
        }

        $explores = Course::select('courses.id', 'courses.name', 'courses.created_at', 'courses.type', 'courses.courseID', 'courses.banner', 'courses.updloaded_by', DB::raw("count(viewers.id) as viewers"))
                        ->join('viewers', 'courses.id', '=', 'viewers.course_id')
                        ->where('is_active', 1)
                        ->whereIn('subject_id', [$lists])
                        ->orWhereIn('courses.id', [$viewer])
                        ->with(array('uploader' => function($query){
                            $query->select('id','name', 'avatar', 'uniqid');
                        }))
                        ->groupBy('courses.id')
                        ->inRandomOrder()
                        ->limit(12)
                        ->get();

    	return view('apps.explore.list', compact('explores'));
    }

    public function subscription()
    {
        $subscription = Subscribe::where('user_id', Auth::id())
                        ->with('creators')
                        ->with(array('creators' => function($query){
                            $query->select('id','name', 'avatar', 'uniqid');
                        }))
                        ->get();

    	return view('apps.subscription.lists', compact('subscription'));
    }

    public function channel($uniqid)
    {
        $user = User::with(array('course' => function($query){
                    $query->select('id','name', 'type', 'banner', 'updloaded_by', 'courseID', 'courses.created_at as times');
                }))
                ->where('is_uploader', true)
                ->where('uniqid', $uniqid)->first();

        $info = $this->infoUploader($user->id, $uniqid);

        $recommended = Course::select('courses.id', 'courses.name', 'courses.created_at', 'courses.courseID', 'courses.type', 'courses.banner', 'courses.updloaded_by')
                        ->where('is_active', 1)
                        ->where('updloaded_by', $user->id)
                        ->with(array('uploader' => function($query){
                            $query->select('id','name', 'avatar', 'uniqid');
                        }))
                        ->inRandomOrder()
                        ->limit(10)
                        ->get();

        $pinned = Course::select('courses.id', 'courses.name', 'courses.created_at', 'courses.courseID', 'courses.description', 'courses.type', 'courses.banner', 'courses.updloaded_by')
                        ->where('is_active', 1)
                        ->where('updloaded_by', $user->id)
                        ->with(array('uploader' => function($query){
                            $query->select('id','name', 'avatar', 'uniqid');
                        }))
                        ->inRandomOrder()
                        ->first();

    	return view('apps.subscription.channel', compact('user', 'info', 'recommended', 'pinned'));
    }

    public function video(Request $request)
    {
        if (!$request->q) {
            return "404";
        }

        $getID = Course::where('courses.courseID', $request->q)->first(); 
        $info = $this->information($getID->id);
        $videos = Course::select('courses.*')
                        ->where('courses.courseID', $request->q)
                        ->with(array('uploader' => function($query){
                            $query->select('id','name', 'avatar', 'uniqid');
                        }))
                        ->with(array('comment' => function($query){
                            $query->orderBy('id','desc');
                        }))
                        ->groupBy('courses.id')
                        ->first();

        $suggest = Course::select('courses.id', 'courses.name', 'courses.type', 'courses.banner', 'courses.courseID')
                        ->where('is_active', 1)
                        ->where('updloaded_by', $videos->updloaded_by)
                        ->inRandomOrder()
                        ->limit(3)
                        ->get();

        return view('apps.course.videos', compact('videos', 'suggest', 'info'));
    }

    public function information($id)
    {
        $course = Course::find($id);
        $view = Viewer::where('course_id', $id)->where('user_id', Auth::id())->first();
        
        if (!$view) {
            Viewer::create([
                'user_id' => Auth::id(),
                'course_id' => $id
            ]);
        }

        $subscription = Subscribe::where('user_id', Auth::id())->where('channelID', $course->updloaded_by)->first();
        $like = Like::where('user_id', Auth::id())->where('course_id', $course->id)->first();

        $info['views'] = Viewer::where('course_id', $id)->count();
        $info['likes'] = Like::where('course_id', $id)->count();
        $info['is_subscribe'] = $subscription? true : false;
        $info['is_liked'] = $like? true : false;
        return $info;
    }

    public function infoUploader($id=0, $uniqid=0)
    {
        $user = User::find($id);
        $course = Course::where('updloaded_by', $id)->pluck('id')->toArray();
        $subscription = Subscribe::where('user_id', Auth::id())->where('channelID', $id)->first();
        $viewers = Viewer::whereIn('course_id', $course)->count();
        $GetMonth = date("m", strtotime($user->created_at));
        $stringMonth = date("F", mktime(0, 0, 0, $GetMonth, 10));
        $date = date(" d, Y", strtotime($user->created_at));

        $info['is_subscribe'] = $subscription? true : false;
        $info['total_views'] = $viewers;
        $info['joined'] = substr($stringMonth, 0,3).$date;
        // return $info['joined'] = date(mktime(0, 0, 0, 09, 10)." d, Y", strtotime($user->created_at));

        return $info;
    }

    public function subscribe(Request $request)
    {  
        $channel = User::where('uniqid', $request->data)->first();
        $subscription = Subscribe::where('user_id', Auth::id())->where('channelID', isset($request->creator)? $request->creator : $channel->id)->first();
        if (!$subscription) {
            Subscribe::create([
                'user_id' => Auth::id(),
                'channelID' => $channel->id
            ]);
            return "added";
        }else {
            Subscribe::where('id', $subscription->id)->delete();
            return "updated";
        }

    }

    public function liked(Request $request)
    {
        $course = Course::where('courseID', $request->data)->first();
        $like = Like::where('user_id', Auth::id())->where('course_id', $course->id)->first();
        if (!$like) {
            Like::create([
                'user_id' => Auth::id(),
                'course_id' => $course->id
            ]);
            return "liked";
        }else {
            Like::where('id', $like->id)->delete();
            return "updated";
        }
    }

    public function followSubjects(Request $request)
    {
        $data = UserSubject::where('user_id', Auth::id())->where('subject_id', $request->data)->first();
        if (!$data) {
            UserSubject::create([
                'user_id' => Auth::id(),
                'subject_id' => $request->data
            ]);
            return "added";
        }else {
            UserSubject::where('user_id', Auth::id())->where('subject_id', $request->data)->delete();
            return "updated";
        }
    }


    public function comment(Request $request)
    {
        Comment::create([
            'user_id' => Auth::id(),
            'course_id' => $request->course_id,
            'description' => $request->text,
        ]);
        return "ok";
    }
}
