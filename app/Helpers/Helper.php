<?php

if (!function_exists('generateRandomString')) {
    /**
     * Returns Random String
     *
     * */
    function generateRandomString($length=20, $params='viedu') {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $names = cleanString($params);
        $charactersLength = strlen($characters);
        $nameLength = strlen($names);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        $randomString .= $names[rand(0, $nameLength - 1)];
        return $randomString;
    }
}

if (!function_exists('cleanString')) {
    /**
     * Removes special chars
     *
     * */
    function cleanString($string) {
        $string = str_replace(' ', '-', $string);
        return preg_replace('/[^A-Za-z0-9\-]/', '', $string);
    }
}



if (!function_exists('interval')) {
    /**
     * Removes special chars
     *
     * */
    function interval($params) {
        $format = date("Y-m-d", strtotime($params));
        $date1 = date_create($format);
        $date2 = date_create(date("Y-m-d"));
        $diff = date_diff($date1,$date2);
        $result = $diff->format("%a");
        if ($result <= 30) {
            return $diff->format("%a days");
        }else if($result >= 31 && $result <= 364){
            return $diff->format("%m Month");
        }else {
            return $diff->format("%y Year");
        }
    }
}