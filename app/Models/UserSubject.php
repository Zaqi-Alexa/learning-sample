<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserSubject extends Model
{
	protected $table = 'user_subjects';

    protected $fillable = [
        'user_id', 'subject_id'
    ];

	function subject(){
		return $this->belongsTo('App\Models\Subject','subject_id');
	}
}
