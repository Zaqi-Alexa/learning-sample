<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    protected $fillable = [
        'name', 'type', 'description', 'banner', 'value', 'views', 'subject_id', 'updloaded_by', 'is_active', 'courseID'
    ];

    function uploader(){
		return $this->belongsTo('App\User','updloaded_by');
	}

	function viewer(){
		return $this->hasMany('App\Models\Viewer','course_id');
	}

	function comment(){
		return $this->hasMany('App\Models\Comment','course_id')
				->select('comments.*', 'users.id as usersID', 'users.name', 'users.avatar')
				->join('users', 'users.id', '=', 'comments.user_id');
	}
}
