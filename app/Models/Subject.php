<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Subject extends Model
{
    protected $fillable = [
        'title', 'description', 'is_active',
    ];

    function mySubject()
    {
    	return $this->hasMany('App\Models\UserSubject','subject_id');
    }
}
