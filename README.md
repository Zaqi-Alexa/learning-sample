# Learning Website and Rest Api

## Installation

**Learning Website** utilizes [Composer](https://getcomposer.org/) and [Bower](http://bower.io/) to manage its dependencies. So, before using **Samples Learning Website**, make sure you have Composer and Bower installed on your machine.

1. Clone this repo
2. Move to the cloned folder
3. `git checkout dev`
4. `composer install`
5. Create a new file (`.env`) based on `.env.example`
6. Set database settings in `.env` (`DB_DATABASE`, `DB_USERNAME`, `DB_PASSWORD`)
7. `php artisan key:generate`
8. `You can skip point 9 - 10 if you have sample-database`
9. `php artisan migrate`
10. `php artisan db:seed`
17. `php artisan serve`
18. Access `http://localhost:8000/` in your browser to access admin page
19. Type `zaqi.alexa@mail.com` in `Email` field and type `1234567890` in `Password` field
20. Click Sign In
