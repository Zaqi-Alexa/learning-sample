<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(BannerSeeder::class);
        // $this->call(AdminSeeder::class);
        // $this->call(SubjectsSeeder::class);
        // $this->call(UserDebugSeeder::class);
        // $this->call(UserSubjectSeeder::class);
        // $this->call(UserUploaderSeeder::class);
        // $this->call(CourseSeeder::class);
        // $this->call(ViewersSeeder::class);
        $this->call(LikeSeeder::class);
        $this->call(ReportSeeder::class);
        $this->call(SubscribeSeeder::class);
        $this->call(CommentSeeder::class);
        // $this->call(TestSeeder::class);
    }
}
