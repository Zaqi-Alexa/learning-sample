<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\File;

use App\Models\Course;

class CourseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$banner = array("1.png", "2.png", "3.png");
        $file = array("sample1.mp4", "sample2.mp4", "sample3.mp4");

        for($i = 1; $i <= 50000; $i++){
	        $faker = Faker\Factory::create();
        	$uniqid = generateRandomString('20', $faker->text(19));
	        $key_val = rand(0,2);
	        Course::create([
	        	'name' => $faker->text(50), 
	        	'type' => 'videos', 
	        	'description' => $faker->text, 
	        	'banner' => $banner[$key_val].'.png', 
	        	'value' => $file[$key_val], 
	        	'courseID' => $uniqid, 
	        	'subject_id' => rand(1,14), 
	        	'updloaded_by' => rand(4,3003)
	        ]);
        }
    }
}
