<?php

use Illuminate\Database\Seeder;
use App\Models\Banner;
use Illuminate\Support\Facades\Hash;

class BannerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	for($i = 1; $i <= 3; $i++){
	        Banner::create([
	            'type' => 'images',
	            'value' => $i.'.png',
	            'is_active' => true
	        ]);
    	}

    	Banner::create([
    		'type' => 'videos',
            'thumbnail' => 'cover.jpg',
            'value' => 'videos.mp4',
            'is_active' => true
        ]);
    }
}
