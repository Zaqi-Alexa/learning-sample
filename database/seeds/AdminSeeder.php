<?php

use Illuminate\Database\Seeder;
use App\User;
use Illuminate\Support\Facades\Hash;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$uniqid = generateRandomString('20', 'VIEDU');
        User::create([
            'name' => 'VIEDU',
            'email' => 'admin@viedu.com',
            'uniqid' => $uniqid,
            'password' => Hash::make('1234567890'),
            'avatar' => $uniqid.'/avatar.png',
            'banner' => $uniqid.'/VIEDU-Banner.png',
            'is_admin' => true,
            'description' => 'I makes videos'
        ]);
    }
}
