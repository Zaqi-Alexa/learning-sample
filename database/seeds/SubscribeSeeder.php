<?php

use Illuminate\Database\Seeder;
use App\Models\Subscribe;

class SubscribeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	for($i = 1; $i <= 40; $i++){
        	Subscribe::create([
	            'channelID' => rand(7,3003),
	            'user_id' => 2,
	        ]);
    	}
    }
}
