<?php

use Illuminate\Database\Seeder;
use App\Models\UserSubject;

class UserSubjectSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $subject = array("Physics", "Chemistry", "Biology", "Mathematics", "English", "History", "Astronomy", "UI/UX Design", "Python Programming", "Spanish", "Laravel", "Spanish", "Economics", "Accounting");

        for($i = 1; $i <= 10; $i++){
	        UserSubject::create([
	        	'user_id' => 2,
				'subject_id' => $i,
	        ]);
        }
    }
}
