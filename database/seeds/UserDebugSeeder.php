<?php

use Illuminate\Database\Seeder;
use App\User;
use Illuminate\Support\Facades\Hash;

class UserDebugSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$uniqid = generateRandomString('20', 'VIEDU');
        User::create([
            'name' => 'Zaqi Alexa',
            'email' => 'zaqi.alexa@mail.com',
            'uniqid' => $uniqid,
            'password' => Hash::make('1234567890'),
        ]);
    }
}
