<?php

use Illuminate\Database\Seeder;
use App\Models\Viewer;

class ViewersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $count = 100000 - 49209;
        for($i = 1; $i <= $count; $i++){
	        Viewer::create([
	            'course_id' => rand(1,50000),
	            'user_id' => rand(2,3003),
	        ]);
    	}
    }
}
