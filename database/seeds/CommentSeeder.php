<?php

use Illuminate\Database\Seeder;
use App\Models\Comment;

class CommentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for($i = 1; $i <= 50000; $i++){
        	$faker = Faker\Factory::create();
	        Comment::create([
	            'course_id' => rand(1,50000),
	            'user_id' => rand(2,3003),
	            'description' => $faker->text,
	        ]);
    	}
    }
}
