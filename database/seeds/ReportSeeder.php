<?php

use Illuminate\Database\Seeder;
use App\Models\Report;

class ReportSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$type = array('Request New Subjects', 'Inappropriate Content');
    	$course = array(null, rand(1,50000));

        for($i = 1; $i <= 100; $i++){
	    	$key = rand(0,1);
	    	$faker = Faker\Factory::create();
        	Report::create([
	        	'user_id' => rand(7,3003),
				'course_id' => $course[$key],
				'type' => $type[$key],
				'description' => $faker->text,
	        ]);
        }
    }
}
