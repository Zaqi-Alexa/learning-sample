<?php

use Illuminate\Database\Seeder;
use App\Models\Subject;

class SubjectsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $subject = array("Physics", "Chemistry", "Biology", "Mathematics", "English", "History", "Astronomy", "UI/UX Design", "Python Programming", "React JS", "Laravel", "Spanish", "Economics", "Accounting");

        for($i = 0; $i <= 13; $i++){
	        Subject::create([
	        	'title'	=> $subject[$i],
                'icon' => $subject[$i].'.png',
				'description'	=> "Lorem Ipsum is simply dummy text of the printing and typesetting industry.",
				'is_active'	=> true
	        ]);
        }
    }
}
