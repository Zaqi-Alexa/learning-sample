<?php

use Illuminate\Database\Seeder;
use App\Models\Like;

class LikeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $count = 236000 - 17767;
        for($i = 1; $i <= $count; $i++){
	        Like::create([
	            'course_id' => rand(1,50000),
	            'user_id' => rand(2,3003),
	        ]);
    	}
    }
}
