<?php

use Illuminate\Database\Seeder;
use App\User;

class UserUploaderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	for($i = 1; $i <= 3000; $i++){
    		$faker = Faker\Factory::create();
	        $uniqid = generateRandomString('20', 'VIEDU');
	        User::create([
	            'name' => $faker->name,
	            'email' => "user".$i.'@mail.com',
	            'uniqid' => $uniqid,
	            'password' => Hash::make('1234567890'),
	            'avatar' => 'avatar.png',
	            'banner' => 'VIEDU-Banner.png',
	            'is_uploader' => true,
	            'description' => 'I makes videos'
	        ]);
    	}
    }
}
