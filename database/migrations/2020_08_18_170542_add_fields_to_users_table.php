<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldsToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('uniqid');
            $table->string('banner')->nullable();
            $table->string('avatar')->nullable();
            $table->string('phone_number', 15)->nullable();
            $table->longText('description')->nullable();
            $table->boolean('is_admin')->default(0);
            $table->boolean('is_uploader')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            //
        });
    }
}
