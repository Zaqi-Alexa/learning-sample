@extends('templateAuth')
@section('title', 'Sign up')
@section('content')
<div class="limiter">
    <div class="container-login100">
        <div class="wrap-login100">
             <form method="post" action="{{ route('register') }}" class="login100-form validate-form">
                @csrf
                <span class="login100-form-title p-b-43">
                    Sign up
                </span>
                @error('email')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
                @error('password')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
                @error('name')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
                <div class="wrap-input-signup100 validate-input" data-validate="Valid name is required">
                    <input class="input100" type="text" name="name" value="{{ old('name') }}" autocomplete="name" autofocus required>
                    <span class="focus-input100"></span>
                    <span class="label-input100">Name</span>
                </div>
                <div class="wrap-input-signup100 validate-input" data-validate="Valid email is required: ex@abc.xyz">
                    <input class="input100" type="text" name="email" value="{{ old('email') }}" required autocomplete="email">
                    <span class="focus-input100"></span>
                    <span class="label-input100">Email</span>
                </div>
                <div class="wrap-input-signup100 validate-input" data-validate="Password is required">
                    <input class="input100" type="password" name="password" required autocomplete="new-password">
                    <span class="focus-input100"></span>
                    <span class="label-input100">Password</span>
                </div>
                <div class="wrap-input-signup100 validate-input" data-validate="Password is required">
                    <input class="input100" type="password" name="password_confirmation" required autocomplete="new-password">
                    <span class="focus-input100"></span>
                    <span class="label-input100">Confirmation Password</span>
                </div>
                <div class="flex-sb-m w-full p-t-3 p-b-32">
                    <div class="contact100-form-checkbox">
                        <a href="#" class="txt1">
                            Forgot Password?
                        </a>
                    </div>
                    <div>
                        <a href="{{ route('login') }}" class="txt1">
                            Already have an Account?
                        </a>
                    </div>
                </div>
                <div class="container-login100-form-btn">
                    <button type="submit" class="login100-form-btn">
                        Sign up
                    </button>
                </div>
                <div class="text-center p-t-46 p-b-20">
                    <span class="txt2">
                        or sign up using
                    </span>
                </div>
                <div class="login100-form-social flex-c-m">
                    <a href="#" class="login100-form-social-item flex-c-m bg1 m-r-5">
                        <i class="fa fa-facebook-f" aria-hidden="true"></i>
                    </a>
                    <a href="#" class="login100-form-social-item flex-c-m bg2 m-r-5">
                        <i class="fa fa-twitter" aria-hidden="true"></i>
                    </a>
                </div>
            </form>
            <div class="login100-more" style="background-image: url({{ asset('assets/authpage/images/singup.png') }});">
            </div>
        </div>
    </div>
</div>
@endsection