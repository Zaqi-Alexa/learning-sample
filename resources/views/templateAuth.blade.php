<!DOCTYPE html>
<html lang="en">

<head>
    <title>Course | @yield('title')</title>
    <meta charset="utf-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

    <link rel="stylesheet" type="text/css" href="{{ asset('assets/authpage/vendor/bootstrap/css/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/authpage/fonts/font-awesome-4.7.0/css/font-awesome.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/authpage/fonts/Linearicons-Free-v1.0.0/icon-font.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/authpage/vendor/animate/animate.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/authpage/vendor/css-hamburgers/hamburgers.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/authpage/vendor/animsition/css/animsition.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/authpage/vendor/select2/select2.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/authpage/vendor/daterangepicker/daterangepicker.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/authpage/css/util.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/authpage/css/main.css') }}">
</head>

<body style="background-color: #666666;">
    
    @yield('content')

    <script src="{{ asset('assets/authpage/vendor/jquery/jquery-3.2.1.min.js') }}"></script>
    <script src="{{ asset('assets/authpage/vendor/animsition/js/animsition.min.js') }}"></script>
    <script src="{{ asset('assets/authpage/vendor/bootstrap/js/popper.js') }}"></script>
    <script src="{{ asset('assets/authpage/vendor/bootstrap/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('assets/authpage/vendor/select2/select2.min.js') }}"></script>
    <script src="{{ asset('assets/authpage/vendor/daterangepicker/moment.min.js') }}"></script>
    <script src="{{ asset('assets/authpage/vendor/daterangepicker/daterangepicker.js') }}"></script>
    <script src="{{ asset('assets/authpage/vendor/countdowntime/countdowntime.js') }}"></script>
    <script src="{{ asset('assets/authpage/js/main.js') }}"></script>
</body>

</html>
