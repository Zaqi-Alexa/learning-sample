@extends('template')
@section('title', '| Welcome')
@section('content')
<div class="page-wrapper">
    <div class="container-fluid pb-0">
        <div class="row">
            <div class="col-10">
                <div id="ctrl" class="carousel slide" data-ride="carousel" style="width: 700px !important;">
                    <div class="carousel-inner">
                        @php($i=1)
                        @foreach ($banners as $banner)
                            @php($status = '')
                            @if($i++ == 1)
                                @php($status = 'active')
                            @endif
                            @if($banner->type == 'images')
                                <div class="carousel-item rounded-10 {{ $status }}">
                                    <div class="d-block">
                                        <img src="{{ asset('banners/banner/'.$banner->value) }}" class="rounded-10" width="700" height="400">
                                    </div>
                                </div>
                            @else
                                <div class="carousel-item {{ $status }}">
                                    <video class="d-block rounded-10 w-100" controls crossorigin playsinline poster="{{ $banner->thumbnail }}" style="width: 700px !important;">
                                        <source src="{{ asset('banners/banner/'.$banner->value) }}">
                                    </video>
                                </div>
                            @endif
                        @endforeach
                    </div>
                    <a style="height: 80%;" class="carousel-control-prev" href="#ctrl" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a style="height: 80%;" class="carousel-control-next" href="#ctrl" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid pb-0">
        <div class="row">
            <div class="col">
                <div class="col-7 align-self-center">
                    <h3 class="page-title text-dark font-weight-medium mb-1">Recomended</h3>
                </div>
            </div>
            <div class="col" style="text-align: right;">
            </div>
        </div>
        <div class="row" style="padding: 30px">
            <div class="owl-carousel owl-theme">

                @foreach($recommended as $r)
                <div class="item item-size-box">
                    <img src="{{ asset('videos/sample/images/'.$r->banner) }}" class="thumbnail" style="min-height: 166px !important;">
                    <a href="{{ url('course/watch?q='.$r->courseID) }}" class="title">{{ $r->name }}</a>
                    <br>
                    <div style="margin-top: 5px;">
                        <a href="{{ url('channel/'.$r->uploader->uniqid) }}" class="uploader">{{ $r->uploader->name }}</a>
                    </div>
                    <div style="margin-top: 5px;">
                        <p class="date-release">{{ $r->viewers }} views • {{ interval($r->created_at) }} ago</p>
                    </div>
                </div>
                @endforeach

            </div>
        </div>
    </div>
    <div class="container-fluid pt-0">
        <div class="row">
            <div class="col">
                <div class="col-7 align-self-center">
                    <img src="{{ asset('account/avatar/'.$channel->avatar) }}" alt="user" class="rounded-circle" width="60">
                    <a href="{{ url('channel/'.$channel->uniqid) }}" class="username">{{ $channel->name }}</a>
                    <p class="label-info">Recommended chanel for you</p>
                </div>
            </div>
        </div>
        <div class="row" style="padding: 30px">
            <div class="owl-carousel owl-theme">
                @foreach($channel->course as $video)
                <div class="item item-size-box">
                    <img src="{{ asset('videos/sample/images/'.$video->banner) }}" class="thumbnail" style="min-height: 166px !important;">
                    <a href="{{ url('course/watch?q='.$video->courseID) }}" class="title">{{ $video->name }}</a>
                </div>
                @endforeach

            </div>
        </div>
    </div>
    <div class="container-fluid pt-0 pb-0">
        <div class="row" style="padding: 30px">
            @foreach($suggest as $s)
            <div class="col-3 item-size-box" style="margin-right: 40px;">
                <img src="{{ asset('videos/sample/images/'.$s->banner) }}" class="thumbnail" style="min-height: 166px !important;">
                <a href="{{ url('course/watch?q='.$s->courseID) }}" class="title">{{ $s->name }}</a>
                <br>
                <div style="margin-top: 5px;">
                    <a href="{{ url('channel/'.$s->uploader->uniqid) }}" class="uploader">{{ $s->uploader->name }}</a>
                </div>
                <div style="margin-top: 5px;">
                    <p class="date-release">{{ $s->viewers }} views • {{ interval($s->created_at) }} ago</p>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</div>
@endsection
@section('js')
<script type="text/javascript">
$('.owl-carousel').owlCarousel({
    loop: true,
    margin: 10,
    nav: true,
    dots: false,
    autoWidth: true,
})
$('.carousel').carousel({
    interval: false
})

</script>
@endsection
