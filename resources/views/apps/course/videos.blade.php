@extends('template')
@section('title', '| Video')
@section('content')
<div class="page-wrapper ml-0">
    <div class="container-fluid">
        <div class="row col-12">
            <div class="col-8">
                <video class="w-100 rounded-10 mb-3" controls crossorigin playsinline poster="{{ asset('videos/sample/images/'.$videos->banner) }}">
                    <source class="d-block rounded-10 w-100" src="{{ asset('videos/sample/'.$videos->value) }}" type="video/mp4">
                </video>
                <div>
                    <div style="display: inline-flex;">
                        <p class="mt-3 mb-4 text-dark" style="font-weight: 500">Watch more videos from </p> &nbsp;
                        <a class="mt-3 mb-4 text-dark" style="font-weight: 500" href="{{ url('channel/'.$videos->uploader->uniqid) }}">{{ $videos->uploader->name }}</a>
                    </div>
                    <div class="row mt-3">
                        @foreach($suggest as $s)
                        <div class="col-4">
                            <a href="{{ url('course/watch?q='.$s->courseID) }}">
                                <div class="item item-size-box">
                                    <img src="{{ asset('videos/sample/images/'.$s->banner) }}" class="thumbnail" style="min-height: 166px !important;">
                                </div>
                            </a>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
            <div class="col-4">
                <h2 class="text-dark" style="font-weight: 500;">{{ $videos->name }}</h2>
                <div class="row justify-content-between ml-0 mt-4 mb-4">
                    <p>{{ $info['views'] }} views</p>
                    <div style="display: inline-flex;">
                        <p id="liked-result">{{ $info['likes'] }} </p>&nbsp;<p> likes</p>
                    </div>
                </div>
                <div class="row ml-0">
                    <img src="https://images.unsplash.com/photo-1518577915332-c2a19f149a75?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=484&q=80" alt="user" class="rounded-circle" width="55">
                    <a href="{{ url('channel/'.$videos->uploader->uniqid) }}" class="text-dark ml-3 justify-content-center" style="font-weight: 500;align-self: center;">{{ $videos->uploader->name }}</a>
                </div>
                <a href="#" id="subs" data-id="{{$videos->uploader->uniqid}}" class="btn {{ $info['is_subscribe']? 'btn-primary': 'btn-outline-primary' }} rounded-10 mt-4">{{ $info['is_subscribe']? 'Subscribed': 'Subscribe' }}</a>

                <a href="#" id="liked" data-id="{{$videos->courseID}}" class="btn {{ $info['is_liked']? 'btn-danger': 'btn-outline-danger' }} rounded-10 mt-4">{!! $info['is_liked']? '<i id="like-icon" class="fas fa-thumbs-up"></i>': '<i id="like-icon" class="far fa-thumbs-up"></i>' !!}</a>
                
                <div class="mt-4">
                    <h3 class="text-dark justify-content-center" style="font-weight: 500;">Description</h3>
                    <p class="text-dark" style="font-size: 14px;">{{ $videos->description }}</p>
                </div>
            </div>
            <div class="col-8 mt-5">
                <h2 class="text-dark justify-content-center mb-5" style="font-weight: 500;">Comments</h2>
                <div class="mb-5">
                    <form>
                        <div class="form-group">
                            <textarea id="textareaID" class="form-control" rows="3" placeholder="Type Something..." required></textarea>
                        </div>
                        <button class="btn btn-primary custom-radius btn-submit">Post Comment</button>
                    </form>
                </div>
                <hr>
                
                @foreach($videos->comment as $comment)
                    <div id="list-comment"></div>
                    <div class="mt-3">
                        <div class="row">
                            <img src="https://images.unsplash.com/photo-1518577915332-c2a19f149a75?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=484&q=80" alt="user" class="rounded-circle" width="40" style="margin-left: 15px;">
                            <h4 class="text-dark ml-3 justify-content-center" style="font-weight: 500;align-self: center;">{{ $comment->name }}</h4>
                        </div>
                        <p class="mt-3 text-dark" style="font-size: 15px;">{{ $comment->description }}.</p>
                    </div>
                    <hr>
                @endforeach
            </div>
        </div>
    </div>
</div>
@endsection
@section('js')
<!-- Js -->
<script type="text/javascript">
    $("#subs").click(function (e) {
        var data = $(this).attr("data-id");
        $.ajax({
            url: 'subscribe/',
            type: 'post',
            data: {"_token": "{{ csrf_token() }}",data:data},
            success: function(response){
                if (response == "added") {
                    document.getElementById("subs").classList.remove('btn-outline-primary');
                    document.getElementById("subs").classList.add('btn-primary');
                    $("#subs").text("Subscribed");
                }else {
                    document.getElementById("subs").classList.remove('btn-primary');
                    document.getElementById("subs").classList.add('btn-outline-primary');
                    $("#subs").text("Subscribe");
                }
            }
        });
    });

    $("#liked").click(function (e) {
        var data = $(this).attr("data-id");
        var total = document.getElementById('liked-result').innerHTML;
        $.ajax({
            url: 'liked/',
            type: 'post',
            data: {"_token": "{{ csrf_token() }}",data:data},
            success: function(response){
                if (response == "liked") {
                    document.getElementById('liked-result').innerHTML = parseInt(total)+1;
                    document.getElementById("liked").classList.remove('btn-outline-danger');
                    document.getElementById("liked").classList.add('btn-danger');
                    document.getElementById("like-icon").classList.remove('far fa-thumbs-up');
                    document.getElementById("like-icon").classList.add('fas fa-thumbs-up');
                }else {
                    document.getElementById('liked-result').innerHTML = parseInt(total)-1;
                    document.getElementById("liked").classList.remove('btn-danger');
                    document.getElementById("liked").classList.add('btn-outline-danger');
                    document.getElementById("like-icon").classList.remove('fas fa-thumbs-up');
                    document.getElementById("like-icon").classList.add('far fa-thumbs-up');
                }
            }
        });
    });
</script>
<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $(".btn-submit").click(function(e){
        e.preventDefault();
        var value = $("#textareaID").val();
        $.ajax({
           type:'POST',
           url:'comment/',
           data:{ text:value, course_id:{{$videos->id}} },
           success:function(data){
            var parent = document.getElementById('list-comment');
            var a = "<div class='mt-3'><div class='row'>";
            var b = "<img src='https://images.unsplash.com/photo-1518577915332-c2a19f149a75?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=484&q=80' alt='user' class='rounded-circle' width='40' style='margin-left: 15px;'>";
            var c = "<h4 class='text-dark ml-3 justify-content-center' style='font-weight: 500;align-self: center;'>{{  Auth::user()->name }}</h4></div>";
            var d = "<p class='mt-3 text-dark' style='font-size: 15px;'>"+value+"</p></div><hr>";
            var newChild = a+b+c+d; 
            parent.insertAdjacentHTML('afterend', newChild);
            document.getElementById("textareaID").value = "";
            swal("Comment has been posted");
           }
        });

    });
</script>
@endsection
