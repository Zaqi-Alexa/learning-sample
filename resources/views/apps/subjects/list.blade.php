@extends('template')
@section('title', '| Physics')
@section('content')
<div class="page-wrapper">
    <div class="container-fluid pb-0">
        <div class="row">
            <div class="col">
                <div class="col-7 align-self-center">
                    <h3 class="page-title text-dark font-weight-medium mb-1">Physics</h3>
                </div>
            </div>
            <div class="col" style="text-align: right;">
            </div>
        </div>
        <div class="row" style="padding: 30px">

            @foreach($subjects as $s)
            <div class="col-3 item-size-box mb-3 mr-4">
                <img src="https://images.unsplash.com/photo-1485846234645-a62644f84728?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60" class="thumbnail">
                <a href="{{ url('course/watch?q='.$s->id) }}" class="title">{{ $s->name }}</a>
                <br>
                <div style="margin-top: 5px;">
                    <a href="{{ url('channel/'.$s->uploader->uniqid) }}" class="uploader">{{ $s->uploader->name }}</a>
                </div>
                <div style="margin-top: 5px;">
                    <p class="date-release">{{ $s->viewers }} views • {{ interval($s->created_at) }} ago</p>
                </div>
            </div>
            @endforeach

        </div>
    </div>
</div>
@endsection

@section('js')
<!-- Js -->
@endsection
