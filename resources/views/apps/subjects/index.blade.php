@extends('template')
@section('title', '| Subjects')
@section('content')
<div class="page-wrapper">
    <div class="container-fluid pb-0">
        <div class="row">
            <div class="col">
                <div class="col-7 align-self-center">
                    <h3 class="page-title text-dark font-weight-medium mb-1">All Subjects</h3>
                </div>
            </div>
            <div class="col" style="text-align: right;">
            </div>
        </div>
        <div class="row" style="padding: 30px">
            
            @foreach($subjects as $key => $subject)
                <div class="col-3 mb-4">
                    <div class="card-subjects">
                        <img src="{{ asset('assets/apps/assets/img/atom.png') }}" class="mb-2" width="90">
                        <br>
                        <a href="{{ url('subject/?q='.$subject->id) }}" class="text-dark text-subject">{{$subject->title}}</a>
                        <br>

                        @if($subjects[$key]['mySubject']->count() > 0)
                            <a onclick="event.preventDefault();follow({!! $subject->id !!});" id="follow-{{ $subject->id }}" href="#" class="btn btn-primary rounded-pill mt-2">Followed</a>
                        @else
                            <a onclick="event.preventDefault();follow({!! $subject->id !!});" id="follow-{{ $subject->id }}" href="#" class="btn btn-outline-primary rounded-pill mt-2">Follow</a>
                        @endif
                    </div>
                </div>
            @endforeach

        </div>
    </div>
</div>
@endsection

@section('js')
<!-- Js -->
<script type="text/javascript">
    function follow($id){
        var data = $id;
        var key = "follow-"+$id;
        $.ajax({
            url: "{{ url('course/subjects/') }}",
            type: 'post',
            data: {"_token": "{{ csrf_token() }}",data:data},
            success: function(response){
                if (response == "added") {
                    document.getElementById(key).classList.remove('btn-outline-primary');
                    document.getElementById(key).classList.add('btn-primary');
                    $("#follow-"+data).text("Followed");
                }else {
                    document.getElementById(key).classList.remove('btn-primary');
                    document.getElementById(key).classList.add('btn-outline-primary');
                    $("#follow-"+data).text("Follow");
                }
            }
        });
    }
</script>
@endsection
