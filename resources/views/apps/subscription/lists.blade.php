@extends('template')
@section('title', '| Subscription')
@section('content')
<div class="page-wrapper">
    <div class="container-fluid pb-0">
        <div class="row">
            <div class="col">
                <div class="col-7 align-self-center">
                    <h3 class="page-title text-dark font-weight-medium mb-1">My Subscription</h3>
                </div>
            </div>
            <div class="col" style="text-align: right;">
            </div>
        </div>
        <div class="row" style="padding: 30px">

            @foreach($subscription as $s)
            <div class="col-3 mb-4" id="parent-{{$s->creators->id}}">
                <div class="card-subjects">
                    <img src="{{ asset('account/avatar/'.$s->creators->avatar) }}" alt="user" class="rounded-circle" width="90">
                    <br>
                    <br>
                    <a href="{{ url('channel/'.$s->creators->uniqid) }}" class="text-dark text-subject">{{ $s->creators->name }}</a>
                    <br>
                    <br>
                    <a href="#" onclick="event.preventDefault();remove({!! $s->creators->id !!});" id="remove-{{ $s->creators->id }}" class="btn btn-primary rounded-pill">Unsubscribe</a>
                </div>
            </div>
            @endforeach

        </div>
    </div>
</div>
@endsection

@section('js')
<!-- Js -->
<script type="text/javascript">
    function remove(id) {
        swal("Unsubscribe this creator ?", {
            icon: "warning",
            buttons: {
              yes: {
                text: "Yes",
                value: "yes"
              },
              no: {
                text: "No",
                value: "no"
              }
            }
        }).then((value) => {
            if (value === "yes") {
                var data = id;
                var key = "parent-"+data;
                $.ajax({
                    url: "{{ url('course/subscribe/') }}",
                    type: 'post',
                    data: {"_token": "{{ csrf_token() }}",creator:data},
                    success: function(response){
                        if (response == "updated") {
                            swal({
                              icon: "success",
                            });
                            document.getElementById(key).remove();
                        }
                    }
                });
            }
            return false;
        });
    }
</script>
@endsection
