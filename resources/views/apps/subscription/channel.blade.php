@extends('template')
@section('title', '| '.$user->name )
@section('content')
<style type="text/css">
.project-tab {
    padding: 10%;
    margin-top: -8%;
}

.project-tab #tabs {
    background: #007b5e;
    color: #eee;
}

.project-tab #tabs h6.section-title {
    color: #eee;
}

.project-tab #tabs .nav-tabs .nav-item.show .nav-link,
.nav-tabs .nav-link.active {
    color: #7b7b7b;
    background-color: transparent;
    border-color: transparent transparent #f3f3f3;
    border-bottom: 3px solid !important;
    font-size: 16px;
    font-weight: bold;
}

.project-tab .nav-link {
    border: 1px solid transparent;
    border-top-left-radius: .25rem;
    border-top-right-radius: .25rem;
    color: #0062cc;
    font-size: 16px;
    font-weight: 600;
}

.project-tab .nav-link:hover {
    border: none;
}

.project-tab thead {
    background: #f3f3f3;
    color: #333;
}

.project-tab a {
    text-decoration: none;
    color: #333;
    font-weight: 600;
}

.item-links {
    color: gray
}

.item-links:hover {
    color: gray;
    opacity: 0.8;
}

</style>
<div class="page-wrapper">
    <div>
        <img src="{{ asset('account/info/'.$user->banner) }}" style="width: 100%;height: 250px;object-fit: cover;">
        <div style="background-color: #343434; width: 100%;height: 100px;">
            <div class="p-3 d-flex" style="justify-content: space-between;">
                <div>
                    <img src="{{ asset('account/avatar/'.$user->avatar) }}" alt="user" class="rounded-circle" width="60" style="margin-right: 20px;">
                    <a href="#" style="color: #f5f5f5;font-size: 20px;">{{ $user->name }}</a>
                </div>
                <div style="align-self: center;">
                    <a href="#" id="subs" data-id="{{$user->uniqid}}" class="btn btn-primary">{{ $info['is_subscribe']? 'Subscribed': 'Subscribe' }}</a>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <nav>
                    <div class="nav nav-tabs nav-fill" id="nav-tab" role="tablist">
                        <a class="nav-item nav-link item-links active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">Home</a>
                        <a class="nav-item nav-link item-links" id="nav-profile-tab" data-toggle="tab" href="#videos" role="tab" aria-controls="nav-profile" aria-selected="false">Videos</a>
                        <a class="nav-item nav-link item-links" id="nav-contact-tab" data-toggle="tab" href="#info" role="tab" aria-controls="nav-contact" aria-selected="false">Info</a>
                    </div>
                </nav>
                <div class="tab-content" id="nav-tabContent">
                    <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                        <div class="p-5 d-flex">
                            <div class="col-expand">
                                <a href="{{ url('course/watch?q='.$pinned->courseID) }}">
                                    <img src="{{ asset('videos/sample/images/'.$pinned->banner) }}" class="hight-thumbnail">
                                </a>
                            </div>
                            <div class="col-5">
                                <div class="pl-4">
                                    <a style="color: black;font-weight: 400;" href="{{ url('course/watch?q='.$pinned->courseID) }}">{{ $pinned->name }}</a>
                                    <p style="color: #4f4d4d;font-size: 14px;">{{ $pinned->description }}</p>
                                </div>
                            </div>
                        </div>
                        <div class="pl-5 pr-5">
                            <div class="align-self-center">
                                <h3 class="page-title text-dark font-weight-medium mb-1">Recomended</h3>
                            </div>
                        </div>
                        <div class="p-5 d-flex">
                            <div class="owl-carousel owl-theme">
                                @foreach($recommended as $r)
                                <div class="item item-size-box">
                                    <img src="{{ asset('videos/sample/images/'.$r->banner) }}" class="thumbnail" style="min-height: 166px !important;">
                                    <a href="{{ url('course/watch?q='.$r->courseID) }}" class="title">{{ $r->name }}</a>
                                    <br>
                                    <div style="margin-top: 5px;">
                                        <a href="{{ url('channel/'.$r->uploader->uniqid) }}" class="uploader">{{ $r->uploader->name }}</a>
                                    </div>
                                    <div style="margin-top: 5px;">
                                        <p class="date-release">{{ interval($r->created_at) }} ago</p>
                                    </div>
                                </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="videos" role="tabpanel" aria-labelledby="nav-profile-tab">
                        <div class="container-fluid pb-0">
                            <div class="row" style="padding: 30px;justify-content: center;">
                                
                                @foreach( $user->course as $u )
                                <div class="col-3 item-size-box mb-3 mr-4">
                                    <a href="{{ url('course/watch?q='.$u->courseID) }}" class="title">
                                        <img src="{{ asset('videos/sample/images/'.$u->banner) }}" class="thumbnail">
                                        <a href="{{ url('course/watch?q='.$u->courseID) }}" class="title">{{ $u->name }}</a>
                                        <br>
                                    </a>
                                    <div style="margin-top: 5px;">
                                        <p class="date-release">{{ interval($u->times) }} ago</p>
                                    </div>
                                </div>
                                @endforeach

                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="info" role="tabpanel" aria-labelledby="nav-contact-tab">
                        <div class="container-fluid p-5">
                            <div class="col-8 mb-5">
                                <h4 style="color: black">Description</h4>
                                <h5 style="color: #4f4d4d;">{{ $user->description }}</h5>
                            </div>
                            <div class="col-8 mb-5">
                                <h4 style="color: black">Total Viewers</h4>
                                <h5 style="color: #4f4d4d;">{{ $info['total_views'] }} Viewers</h5>
                            </div>
                            <div class="col-8">
                                <h4 style="color: black">Status</h4>
                                <h5 style="color: #4f4d4d;">Joined {{ $info['joined'] }}</h5>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('js')
<!-- Js -->
<script type="text/javascript">
    $('.owl-carousel').owlCarousel({
        loop: true,
        margin: 10,
        nav: true,
        dots: false,
        autoWidth: true,
    })

    $("#subs").click(function (e) {
        var data = $(this).attr("data-id");
        $.ajax({
            url: "{{ url('course/subscribe/') }}",
            type: 'post',
            data: {"_token": "{{ csrf_token() }}",data:data},
            success: function(response){
                if (response == "added") {
                    $("#subs").text("Subscribed");
                }else {
                    $("#subs").text("Subscribe");
                }
            }
        });
    });

</script>
@endsection
