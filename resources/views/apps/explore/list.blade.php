@extends('template')
@section('title', '| Explore')
@section('content')
<div class="page-wrapper">
    <div class="container-fluid pb-0">
        <div class="row">
            <div class="col">
                <div class="col-7 align-self-center">
                    <h3 class="page-title text-dark font-weight-medium mb-1">Explore Videos</h3>
                </div>
            </div>
            <div class="col" style="text-align: right;">
            </div>
        </div>
        <div class="row" style="padding: 30px">

            @foreach($explores as $explore)
            <div class="col-3 item-size-box mb-3 mr-4">
                <img src="{{ asset('videos/sample/images/'.$explore->banner) }}" class="thumbnail" style="min-height: 166px !important;">
                <a href="{{ url('course/watch?q='.$explore->courseID) }}" class="title">{{ $explore->name }}</a>
                <br>
                <div style="margin-top: 5px;">
                    <a href="{{ url('channel/'.$explore->uploader->uniqid) }}" class="uploader">{{ $explore->uploader->name }}</a>
                </div>
                <div style="margin-top: 5px;">
                    <p class="date-release">{{ $explore->viewers }} views • {{ interval($explore->created_at) }} ago</p>
                </div>
            </div>
            @endforeach

        </div>
    </div>
</div>
@endsection

@section('js')
<!-- Js -->
@endsection
